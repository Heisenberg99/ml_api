from flask import Flask, jsonify, request
from tensorflow import keras
import numpy as np
from flask_cors import CORS
import sqlite3
import image


app = Flask(__name__)

model = keras.models.load_model('model.h5')

# Cross Origin Resource Sharing (CORS) handling
CORS(app, resources={'/image': {"origins": "http://localhost:8080"}})

@app.route('/image', methods=['POST'])
def image_post_request():
    x = image.convert(request.json['image'])
    y = model.predict(x.reshape((1,28,28,1))).reshape((10,))
    n = int(np.argmax(y, axis=0))
    y = [float(i) for i in y]

    conn = sqlite3.connect('database.db')
    curs = conn.cursor()
    curs.execute(
        """
        CREATE TABLE IF NOT EXISTS requests(
            digit_id INT PRIMARY KEY,
            digit INT,
            confidence TEXT
        );
        """
    )
    curs.execute("SELECT MAX(digit_id) FROM requests;")
    conn.commit()
    digit_id = curs.fetchone()[0]
    digit_id = 0 if digit_id is None else digit_id + 1
    confidence = y[n]

    curs.execute(
        f"""
        INSERT INTO requests(digit_id, digit, confidence) 
        VALUES('{digit_id}', '{n}', '{confidence}');
        """
    )
    conn.commit()

    return jsonify({'result':y, 'digit':n})


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=6000)
